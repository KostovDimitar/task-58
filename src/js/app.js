import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });

  const navbar = document.createElement('div');
  navbar.setAttribute('class', 'navbar');
  document.body.append(navbar);
  console.log(navbar);
});
